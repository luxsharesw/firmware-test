﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="17008000">
	<Property Name="NI.Lib.ContainingLib" Type="Str">Firmware Test.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../../Firmware Test.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+O!!!*Q(C=T:6,CF.2%)&lt;`C!U//X-2%XL=EVK"GA6)1\:1#X!3[&amp;')))E4RQ5OQ-Y7MI8;A'!'AO.MY@L&gt;=U]?^C/"RB&lt;04&gt;X=]V?&gt;KO]]=C/6^E*[LO:5/TP;$PUFOOUX$`C\7`/AP_U=]Z^N%TT/@T``K;;D\2`ZGW+&amp;JD!V6@P48T`\`N\@T;4::NH.\.\R$_8@VL_&lt;`[HH@],@\#&gt;_C\$:T;T2Q6-\FRKLAS7N4UU./TL_DL]GW&gt;^W`C?@`T(`[?-^0@UD?.IG^630:7+"/7&lt;&gt;$C6[IC&gt;[IC&gt;[IA&gt;[I!&gt;[I!&gt;[I$O[ITO[ITO[IRO[I2O[I2O[&lt;&lt;=38?B#FZ8.3YIHB:+E39)E'"1FNY1HY5FY%B[[3HA3HI1HY5FY#&amp;(#E`!E0!F0QM-Q*4Q*4]+4]#1]J#IE73ZU?")?UCPA#8A#HI!HY+'E!JY!)#A7*![3A+(!'4Q%0!&amp;0Q-/D!J[!*_!*?!)?X!JY!J[!*_!*?"B36C5+48OBQU-;/4Q/D]0D]$A]J*&lt;$Y`!Y0![0QU-Z/4Q/DQ0B&amp;(33AS"HE"0A&gt;"Q?BY=P/4Q/D]0D]$A]O-I/?6G:FK;^U/%R?!Q?A]@A-8B))90(Y$&amp;Y$"[$B\1S?!Q?A]@A-8AI*90(Y$&amp;Y$"#D+/6F*$-''E''90$Q+;@&amp;SCZ&amp;)&lt;&amp;SV:^G&gt;V"6$[$KQ6)^-+I(187$64&gt;/&gt;5.5&amp;VJV!6582H8#KB.2"61NL*J1.6!&lt;\GNMB3WR/4&lt;"RNA)'W,^&gt;OB@$NRM.FKPVVKN6FIOFZL0ZZJ-*BK0RRK.2BI/B_LX_\OXV7OOLP7W\[5JT^.8]^\U`&amp;NP_B)\@^]:_MZ`9,0,[](M9D&lt;Y_'PV=X&lt;R?4#\`$#Y(@0JT&gt;@&amp;YOX6^S`P@NTQ@&lt;/YPFL=DOH?3``$OV(0OH_E`2L^"H2&amp;L.M!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.5</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"-05F.31QU+!!.-6E.$4%*76Q!!$PA!!!18!!!!)!!!$NA!!!!L!!!!!B.';8*N&gt;W&amp;S:3"5:8.U,GRW&lt;'FC%EBJ:WAA17RB=GUO&lt;(:D&lt;'&amp;T=Q!!!!#A&amp;Q#!!!!Q!!!I!!!!!!!!"!!$!$Q!P!!@1)!#!!!!!!%!!1!'`````Q!!!!!!!!!!!!!!!+#)?T$I/SJ0I=7:E5R2$G5!!!!-!!!!%!!!!!"#J[`&amp;R[AR4KL7Z1;/06SPV"W-W9]!MA4JA!G9\0B#@A!!%!!!!!!!A'L/9/WA:5?C%L(_JMIK"1%!!!$`````V"W-W9]!MA4JA!G9\0B#@A!!!"$H9QD1CX[J)6T7WCL2:&amp;RJ!!!!"!!!!!!!!!!H!!&amp;-6E.$!!!!!1!#6EF-1A!!!!"16%AQ!!!!"1!"!!%!!!!!!A!$!!!!!!)!!1!!!!!!)A!!!"RYH'.A9W"K9,D!!-3-1-T5Q01$S0Y!YD-!!'A"#$9!!!!!!%5!!!%9?*RD9-!%`Y%!3$%S-$#&gt;!.)M;/*A'M;G*M"F,C[\I/,-5$?S1E1:A7*-?Y!-*J!=KF[1@]"_1A&amp;M7-Q'!(&lt;V+"5!!!!!!!!-!!&amp;73524!!!!!!!$!!!":!!!!MRYH%NA:'$).,9QWQ#EG9&amp;9H+'")4E`*:7,!=BHA)!N4!Q5AQ#I?6JIYI9($K="A2[`@!O9X`S'J^N&amp;2;#Z2E7#K63EWU&gt;&amp;J..(B;742?8&amp;H````T=@Y4H=\:&amp;TX.%'J,;&lt;!SB_X%7&amp;!]1"UCQA_H^A"EA6T,Q!I'E=$28+$#5MBA?C$B^P-''%7!QT-AL6`BUA[=0)\O%!/E,AY%/7\E9.),^X)IA%#P&amp;UBH")((@BU"%$]BF0&gt;!+N\_3"O:)$&lt;H]9S)!3&amp;9&amp;/%Z",75#GA^6UMRVXU!#\WU%%1G6!K!I)61"W$.A&amp;2TDC$M0$;_XL?\N!Y=C'&amp;)9/1.Q!R+!Y2-:[$)Q-)!O:A'1N6+U.E-U%&amp;90&amp;"9B^!=L71.,T"=F]E"[1T"KI')C^#=JOA,I(*0983%_!ME'_49#SO9(M"6#W%*!N!'6,!NE0I'QZ+(M$.)JQU=\_,KZ)XI?H4Q!V(8+!!!!!$B="A!9!!!9R.SYQ,D%!!!!!!!!-&amp;Q#!!!!!"$%X,D!!!!!!$B="A!9!!!9R.SYQ,D%!!!!!!!!-&amp;Q#!!!!!"$%X,D!!!!!!$B="A!9!!!9R.SYQ,D%!!!!!!!!5!1!!!068.9*Z*K+-,H.34A:*/:U!!!!.!!!!!!!!!!!!!!!!!!!!!!!!!)$`````A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!9!!"A:A!!99'!!'9!9!"I!"!!&lt;!!Q!'M!U!"IQ\!!;$V1!'A+M!"I$6!!;!KQ!'A.5!"I#L!!;!V1!'9+Y!"BD9!!9'Y!!'!9!!"`````Q!!"!$```````````````````````````````````````````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!6&amp;1!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!6L'3)L"5!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!6L'2!1%"!C+Q6!!!!!!!!!!!!!!!!!!!!!0``!!!6L'2!1%"!1%"!1)CM&amp;1!!!!!!!!!!!!!!!!!!``]!C'2!1%"!1%"!1%"!1%#)L!!!!!!!!!!!!!!!!!$``Q"E:%"!1%"!1%"!1%"!10_)!!!!!!!!!!!!!!!!!0``!'3)C'2!1%"!1%"!10```W1!!!!!!!!!!!!!!!!!``]!:)C)C)BE1%"!10``````:!!!!!!!!!!!!!!!!!$``Q"EC)C)C)C):+T```````^E!!!!!!!!!!!!!!!!!0``!'3)C)C)C)C)`````````W1!!!!!!!!!!!!!!!!!``]!:)C)C)C)C)D`````````:!!!!!!!!!!!!!!!!!$``Q"EC)C)C)C)C0````````^E!!!!!!!!!!!!!!!!!0``!'3)C)C)C)C)`````````W1!!!!!!!!!!!!!!!!!``]!:)C)C)C)C)D`````````:!!!!!!!!!!!!!!!!!$``Q#)C)C)C)C)C0```````YC)!!!!!!!!!!!!!!!!!0``!!"E:)C)C)C)`````YCM:!!!!!!!!!!!!!!!!!!!``]!!!!!:)C)C)D``YC):!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!'3)C)C)1!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!"E1!!!!!!!!!!!!!!!!!!!!!!!!!!!````````````````````````````````````````````!!!!$!!"2F")5!!!!!!!!Q!!!HQ!!!4J?*SNF%VI%U%9BL]*7ZG%&amp;G&gt;D;\P1EBCWN9A"5&gt;1;`#FW+F2+57+BBY!*&lt;PS"VGB_2"$;SS,EU*/1A\&lt;8?031AX=*!&gt;F$?\*AQ&gt;#!&gt;S^&amp;Q7\C.Z0OZK=9,S;Q,-M]XTPTPC]$I(RH)ZY[L.J!W$[_T.PA-SQ#5!Z4/0R.P!;W3(Y$'&gt;3)$&gt;.UE?V[[G45BH\$GK$H?"Z_Y/L'6O-6&amp;%G)\?(39UT$94Y&lt;DBP7M(J&lt;LT,^Q[C?\X/GKB"A[[4O7&gt;)$0_G[G5*"-#@&amp;5QW4/B"_3F(-U*X%U[3JC[`?-.8E3+].D&amp;M$;&lt;V['C?C^#=ZEB3A1NY\)Q&amp;(4E+J6'J";B/;E.O9%EQ&amp;R_[11A`'T[WRD&amp;Y^+RG@:&amp;"HR^(BA@I&lt;M8="&gt;;-HO-512?[F=_3+I]5$^L4E;L5;=PA]Z"\&lt;-+B8&lt;V+.\BF@_Q&lt;OGB_"!#H@&amp;^;_%\S[)&amp;+1;`U9R(C%M#C_2WUY9VK?/&amp;-=L2RMS"A5*Y;L)I:&lt;-A9PTT.IZ&lt;$RDRSY5A[.T3TH-NFE/JB['(SQH-BEAM`34VYEMMGAE=AGDC:UD6P;"8&amp;[)3&lt;,!5/AQ(/)N\O&gt;AG+RC!&lt;AMY6?2X29L\K=+E^DO)Y&lt;,=?&amp;;MOZ'_A=`X8PE8"P0/*R/AONNJ\(NKKM?U-&gt;&lt;&lt;XY`^N[#5OUWN67C/&amp;`J5@T,D?BNL&lt;'E&amp;K"7!^G#JGVDL:+&amp;5?H6VOP('VLT.&amp;SWVII&amp;$IY^$TCNF5BJ.F7=\NRU$D!TK+^#M1B,DM,XI7WH&gt;0&gt;3M[^8'B5ZQWK`8U"GU7R.2O'_)R`C]XBN(Y[2W@:*K;+V^&amp;*OEHXSV`=[[=]\Z,G.HQGX`3X!4LS"`K&gt;;KA!!!!%!!!!)!!!!!1!!!!!!!!!$!!"1E2)5!!!!!!!!Q!!!')!!!"S?*RD9'$)%Z"A_M&gt;1^Z?"3?!LE#(^FY&amp;:U)`R.Q-$JZ`!93$.+#!*&amp;*&lt;^S]!OK!U7VD[CS]%!";JMD"S3()=&amp;/=!S(#U;$0```_@Y?O1;8-52(TB4::9]BQ1!&amp;'):!!!!!!!!"!!!!!=!!!(5!!!!"A!!!#&amp;@&lt;GF@4'&amp;T&gt;%NO&lt;X&gt;O4X&gt;O;7ZH4&amp;:$&lt;'&amp;T=U.M&gt;8.U:8)!!!!_&amp;Q#!!!!!!!%!#!!Q`````Q!"!!!!!!!C!!!!!1!;1&amp;!!!"*);7&gt;I)%&amp;M98*N,GRW9WRB=X-!!!%!!!!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'&amp;C4X*E:8)!!!!F&amp;Q#!!!!!!!)!"1!(!!!-!%!!!@````]!!!!"!!%!!!!!!!!!!!!!!"N-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5;7VF=X2B&lt;8!!!!!:&amp;Q#!!!!!!!%!"1!(!!!"!!$87*L+!!!!!!!!!#:-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;-98.U18"Q&lt;'FF:&amp;2J&lt;76T&gt;'&amp;N=!!!!"E8!)!!!!!!!1!&amp;!!=!!!%!!.&gt;9GMI!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962Z='6%:8.D!!!!0B=!A!!!!!!"!!A!-0````]!!1!!!!!!)A!!!!%!'E"1!!!33'FH;#""&lt;'&amp;S&lt;3ZM&gt;G.M98.T!!!"!!!!!!!!!!!!(ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B5WF[:1!!!"E8!)!!!!!!!1!&amp;!!-!!!%!!!!!!!!!!!!!!!!!"!!#!!A!!!!%!!!!1!!!!#A!!!!#!!!%!!!!!!%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\1!!!7FYH)W005Y$-2#&amp;0]?"`%%)39XEAI++BAMM1IIIIYC#%J/VQUK/&amp;HG&gt;1-EZ/")8Y"L*$:BMAE#C15];?4\0P'=$)TS&lt;T`PX$U#0RE6=P.DIT*WLUG69B?*R?&amp;P-H]RVM(%B9":M6@6`I6E+8'14/,M*SSKZ;%JP[CHT()O64=\E.FE/2(4*1;V2UOB^SZ5=/VE,\=/=&gt;P;GSWH;!6&gt;["O+N:@S1FF\GHOYW3[@88,9H.'D4Y@R@_1VK'SE+78O1[#:D-4%==3RQK\[%@5PN^:@]X*T5WHV%-2"(Z%&amp;)7&amp;.IDV/'5HN@/_U]M1!!!!!!!)Q!!1!#!!-!"!!!!%A!$Q!!!!!!$Q$N!/-!!!"?!!]!!!!!!!]!\1$D!!!!&gt;!!0!!!!!!!0!/U!YQ!!!)K!!)!!A!!!$Q$N!/-647FD=G^T&lt;W:U)%JI:7ZH3'6J)&amp;6*&amp;5VJ9X*P=W^G&gt;#"+;'6O:UBF;3"6326.;7.S&lt;X.P:H1A3GBF&lt;G&gt;):7EA65E"-&amp;*45E-.#A!$4&amp;:$1UR#6F=!!!\Y!!!%&amp;Q!!!#!!!!\9!!!!!!!!!!!!!!!A!!!!.!!!"!A!!!!&lt;4%F#4A!!!!!!!!&amp;54&amp;:45A!!!!!!!!&amp;I5F242Q!!!!!!!!&amp;]1U.46!!!!!!!!!'14%FW;1!!!!!!!!'E1U^/5!!!!!!!!!'Y6%UY-!!!!!!!!!(-2%:%5Q!!!!!!!!(A4%FE=Q!!!!!!!!(U6EF$2!!!!!!!!!))&gt;G6S=Q!!!!1!!!)=5U.45A!!!!!!!!+!2U.15A!!!!!!!!+535.04A!!!!!!!!+I;7.M/!!!!!!!!!+]4%FG=!!!!!!!!!,12F")9A!!!!!!!!,E2F"421!!!!!!!!,Y6F"%5!!!!!!!!!--4%FC:!!!!!!!!!-A1E2)9A!!!!!!!!-U1E2421!!!!!!!!.)6EF55Q!!!!!!!!.=2&amp;2)5!!!!!!!!!.Q466*2!!!!!!!!!/%3%F46!!!!!!!!!/96E.55!!!!!!!!!/M2F2"1A!!!!!!!!0!!!!!!0````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Q!!!!!!!!!!$`````!!!!!!!!!.1!!!!!!!!!!0````]!!!!!!!!![!!!!!!!!!!!`````Q!!!!!!!!$Q!!!!!!!!!!$`````!!!!!!!!!2Q!!!!!!!!!!0````]!!!!!!!!"*!!!!!!!!!!!`````Q!!!!!!!!&amp;-!!!!!!!!!!$`````!!!!!!!!!:A!!!!!!!!!!0````]!!!!!!!!"K!!!!!!!!!!%`````Q!!!!!!!!-1!!!!!!!!!!@`````!!!!!!!!!S1!!!!!!!!!#0````]!!!!!!!!$.!!!!!!!!!!*`````Q!!!!!!!!.)!!!!!!!!!!L`````!!!!!!!!!VA!!!!!!!!!!0````]!!!!!!!!$&lt;!!!!!!!!!!!`````Q!!!!!!!!/%!!!!!!!!!!$`````!!!!!!!!!ZA!!!!!!!!!!0````]!!!!!!!!%(!!!!!!!!!!!`````Q!!!!!!!!AA!!!!!!!!!!$`````!!!!!!!!#$!!!!!!!!!!!0````]!!!!!!!!+M!!!!!!!!!!!`````Q!!!!!!!!KY!!!!!!!!!!$`````!!!!!!!!#M!!!!!!!!!!!0````]!!!!!!!!+U!!!!!!!!!!!`````Q!!!!!!!!MY!!!!!!!!!!$`````!!!!!!!!#U!!!!!!!!!!!0````]!!!!!!!!.'!!!!!!!!!!!`````Q!!!!!!!!UA!!!!!!!!!!$`````!!!!!!!!$3A!!!!!!!!!!0````]!!!!!!!!.6!!!!!!!!!#!`````Q!!!!!!!!Z)!!!!!!Z);7&gt;I)%&amp;M98*N,G.U&lt;!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!B.';8*N&gt;W&amp;S:3"5:8.U,GRW&lt;'FC%EBJ:WAA17RB=GUO&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!!!!!!!!!!!!'!!%!!!!!!!!!!!!!!1!;1&amp;!!!"*);7&gt;I)%&amp;M98*N,GRW9WRB=X-!!!%!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!,``Q!!!!%!!!!!!!%!!!!!!1!;1&amp;!!!"*);7&gt;I)%&amp;M98*N,GRW9WRB=X-!!!%!!!!!!!(````_!!!!!!)42GFS&lt;8&gt;B=G5A6'6T&gt;#ZM&gt;GRJ9B6';8*N&gt;W&amp;S:3"5:8.U,GRW9WRB=X.16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!,``Q!"!!!!!!!#!!!!!!%!'E"1!!!33'FH;#""&lt;'&amp;S&lt;3ZM&gt;G.M98.T!!!"!!!!!!!"`````A!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!8!)!!!!!!!!!!!P``!!!!!1!!!!!!!Q!!!!!"!"J!5!!!%EBJ:WAA17RB=GUO&lt;(:D&lt;'&amp;T=Q!!!1!!!!!!!@````Y!!!!!!B.';8*N&gt;W&amp;S:3"5:8.U,GRW&lt;'FC&amp;5:J=GVX98*F)&amp;2F=X1O&lt;(:D&lt;'&amp;T=V"53$!!!!!!!!!!!!!8!)!!!!!!!!!!!P``!!%!!!!!!!1!!!!!!1!;1&amp;!!!"*);7&gt;I)%&amp;M98*N,GRW9WRB=X-!!!%!!!!!!!(````_!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"=!A!!!!!!!!!!#``]!!!!"!!!!!!!&amp;!!!!!!%!'E"1!!!33'FH;#""&lt;'&amp;S&lt;3ZM&gt;G.M98.T!!!"!!!!!!!"`````A!!!!!#%U:J=GVX98*F)&amp;2F=X1O&lt;(:M;7)62GFS&lt;8&gt;B=G5A6'6T&gt;#ZM&gt;G.M98.T5&amp;2)-!!!!!!!!!!!!"=!A!!!!!!!!!!!!!!!</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.5</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"A!!!!!B.';8*N&gt;W&amp;S:3"5:8.U,GRW&lt;'FC&amp;5:J=GVX98*F)&amp;2F=X1O&lt;(:D&lt;'&amp;T=V"53$!!!!!K!!%!"!!!$5:J=GVX98*F)&amp;2F=X162GFS&lt;8&gt;B=G5A6'6T&gt;#ZM&gt;G.M98.T!!!!!!</Property>
	<Item Name="High Alarm.ctl" Type="Class Private Data" URL="High Alarm.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Override" Type="Folder">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Do.vi" Type="VI" URL="../Do.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%`!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%*!=!!?!!!I%U:J=GVX98*F)&amp;2F=X1O&lt;(:M;7)33'FH;#""&lt;'&amp;S&lt;3ZM&gt;G.M98.T!!!/3'FH;#""&lt;'&amp;S&lt;3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!1%"Q!"Y!!#A42GFS&lt;8&gt;B=G5A6'6T&gt;#ZM&gt;GRJ9B*);7&gt;I)%&amp;M98*N,GRW9WRB=X-!!!V);7&gt;I)%&amp;M98*N)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!#!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
		</Item>
		<Item Name="Temp Offset.vi" Type="VI" URL="../Temp Offset.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%`!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%*!=!!?!!!I%U:J=GVX98*F)&amp;2F=X1O&lt;(:M;7)33'FH;#""&lt;'&amp;S&lt;3ZM&gt;G.M98.T!!!/3'FH;#""&lt;'&amp;S&lt;3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!1%"Q!"Y!!#A42GFS&lt;8&gt;B=G5A6'6T&gt;#ZM&gt;GRJ9B*);7&gt;I)%&amp;M98*N,GRW9WRB=X-!!!V);7&gt;I)%&amp;M98*N)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!#!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
		</Item>
		<Item Name="Temp Flag.vi" Type="VI" URL="../Temp Flag.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;*!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!J!)12'&lt;'&amp;H!!"#1(!!(A!!+".';8*N&gt;W&amp;S:3"5:8.U,GRW&lt;'FC%EBJ:WAA17RB=GUO&lt;(:D&lt;'&amp;T=Q!!$EBJ:WAA17RB=GUA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I%U:J=GVX98*F)&amp;2F=X1O&lt;(:M;7)33'FH;#""&lt;'&amp;S&lt;3ZM&gt;G.M98.T!!!.3'FH;#""&lt;'&amp;S&lt;3"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
		<Item Name="Voltage Offset.vi" Type="VI" URL="../Voltage Offset.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%`!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%*!=!!?!!!I%U:J=GVX98*F)&amp;2F=X1O&lt;(:M;7)33'FH;#""&lt;'&amp;S&lt;3ZM&gt;G.M98.T!!!/3'FH;#""&lt;'&amp;S&lt;3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!1%"Q!"Y!!#A42GFS&lt;8&gt;B=G5A6'6T&gt;#ZM&gt;GRJ9B*);7&gt;I)%&amp;M98*N,GRW9WRB=X-!!!V);7&gt;I)%&amp;M98*N)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!#!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
		<Item Name="Voltage Flag.vi" Type="VI" URL="../Voltage Flag.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;*!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!J!)12'&lt;'&amp;H!!"#1(!!(A!!+".';8*N&gt;W&amp;S:3"5:8.U,GRW&lt;'FC%EBJ:WAA17RB=GUO&lt;(:D&lt;'&amp;T=Q!!$EBJ:WAA17RB=GUA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I%U:J=GVX98*F)&amp;2F=X1O&lt;(:M;7)33'FH;#""&lt;'&amp;S&lt;3ZM&gt;G.M98.T!!!.3'FH;#""&lt;'&amp;S&lt;3"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
		<Item Name="Tx Bias Offset.vi" Type="VI" URL="../Tx Bias Offset.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%`!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%*!=!!?!!!I%U:J=GVX98*F)&amp;2F=X1O&lt;(:M;7)33'FH;#""&lt;'&amp;S&lt;3ZM&gt;G.M98.T!!!/3'FH;#""&lt;'&amp;S&lt;3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!1%"Q!"Y!!#A42GFS&lt;8&gt;B=G5A6'6T&gt;#ZM&gt;GRJ9B*);7&gt;I)%&amp;M98*N,GRW9WRB=X-!!!V);7&gt;I)%&amp;M98*N)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!#!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
		<Item Name="Tx Bias Flags.vi" Type="VI" URL="../Tx Bias Flags.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;B!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!)1J);7&gt;I)%&amp;M98*N!!!31%!!!@````]!"16'&lt;'&amp;H=Q"#1(!!(A!!+".';8*N&gt;W&amp;S:3"5:8.U,GRW&lt;'FC%EBJ:WAA17RB=GUO&lt;(:D&lt;'&amp;T=Q!!$EBJ:WAA17RB=GUA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I%U:J=GVX98*F)&amp;2F=X1O&lt;(:M;7)33'FH;#""&lt;'&amp;S&lt;3ZM&gt;G.M98.T!!!.3'FH;#""&lt;'&amp;S&lt;3"J&lt;A"5!0!!$!!$!!1!"A!(!!1!"!!%!!1!#!!%!!1!#1-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!I!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
		<Item Name="Tx Power Offset.vi" Type="VI" URL="../Tx Power Offset.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%`!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%*!=!!?!!!I%U:J=GVX98*F)&amp;2F=X1O&lt;(:M;7)33'FH;#""&lt;'&amp;S&lt;3ZM&gt;G.M98.T!!!/3'FH;#""&lt;'&amp;S&lt;3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!1%"Q!"Y!!#A42GFS&lt;8&gt;B=G5A6'6T&gt;#ZM&gt;GRJ9B*);7&gt;I)%&amp;M98*N,GRW9WRB=X-!!!V);7&gt;I)%&amp;M98*N)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!#!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
		<Item Name="Tx Power Flags.vi" Type="VI" URL="../Tx Power Flags.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;B!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!)1J);7&gt;I)%&amp;M98*N!!!31%!!!@````]!"16'&lt;'&amp;H=Q"#1(!!(A!!+".';8*N&gt;W&amp;S:3"5:8.U,GRW&lt;'FC%EBJ:WAA17RB=GUO&lt;(:D&lt;'&amp;T=Q!!$EBJ:WAA17RB=GUA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I%U:J=GVX98*F)&amp;2F=X1O&lt;(:M;7)33'FH;#""&lt;'&amp;S&lt;3ZM&gt;G.M98.T!!!.3'FH;#""&lt;'&amp;S&lt;3"J&lt;A"5!0!!$!!$!!1!"A!(!!1!"!!%!!1!#!!%!!1!#1-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!I!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
		</Item>
		<Item Name="Rx Power Offset.vi" Type="VI" URL="../Rx Power Offset.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%`!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%*!=!!?!!!I%U:J=GVX98*F)&amp;2F=X1O&lt;(:M;7)33'FH;#""&lt;'&amp;S&lt;3ZM&gt;G.M98.T!!!/3'FH;#""&lt;'&amp;S&lt;3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!1%"Q!"Y!!#A42GFS&lt;8&gt;B=G5A6'6T&gt;#ZM&gt;GRJ9B*);7&gt;I)%&amp;M98*N,GRW9WRB=X-!!!V);7&gt;I)%&amp;M98*N)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!#!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
		<Item Name="Rx Power Flags.vi" Type="VI" URL="../Rx Power Flags.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;B!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!)1J);7&gt;I)%&amp;M98*N!!!31%!!!@````]!"16'&lt;'&amp;H=Q"#1(!!(A!!+".';8*N&gt;W&amp;S:3"5:8.U,GRW&lt;'FC%EBJ:WAA17RB=GUO&lt;(:D&lt;'&amp;T=Q!!$EBJ:WAA17RB=GUA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I%U:J=GVX98*F)&amp;2F=X1O&lt;(:M;7)33'FH;#""&lt;'&amp;S&lt;3ZM&gt;G.M98.T!!!.3'FH;#""&lt;'&amp;S&lt;3"J&lt;A"5!0!!$!!$!!1!"A!(!!1!"!!%!!1!#!!%!!1!#1-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!I!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
	</Item>
	<Item Name="SubVIs" Type="Folder">
		<Item Name="Get Display Data.vi" Type="VI" URL="../Get Display Data.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#/!!!!"1!%!!!!#!!Q`````Q!=1%!!!@````]!!1Z%;8.Q&lt;'&amp;Z)%2B&gt;'&amp;&lt;81!!#E!B"%:M97=!!&amp;1!]!!-!!!!!!!!!!)!!!!!!!!!!!!!!!!!!!!$!A!!?!!!!!!!!!!!!!!!!!!!#1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!%!"!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
		</Item>
		<Item Name="Get Display Data[].vi" Type="VI" URL="../Get Display Data[].vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#G!!!!"A!%!!!!#!!Q`````Q!=1%!!!@````]!!1Z%;8.Q&lt;'&amp;Z)%2B&gt;'&amp;&lt;81!!%%!B#EBJ:WAA17RB=GU!!"*!1!!"`````Q!$"5:M97&gt;T!&amp;1!]!!-!!!!!!!!!!)!!!!!!!!!!!!!!!!!!!!%!Q!!?!!!!!!!!!!!!!!!!!!!#1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!))!!!!!!%!"1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
		</Item>
		<Item Name="Set Product Tx Power Offset.vi" Type="VI" URL="../Set Product Tx Power Offset.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;Z!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%Z!=!!?!!!Q&amp;E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9H!84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-!!".0=(2J9W&amp;M)&amp;"S&lt;W2V9X1A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%U!+!!R0:G:T:81A6G&amp;M&gt;75!!%Z!=!!?!!!Q&amp;E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9H!84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-!!"*0=(2J9W&amp;M)&amp;"S&lt;W2V9X1A;7Y!!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!A!!!!+!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342972432</Property>
		</Item>
	</Item>
</LVClass>
